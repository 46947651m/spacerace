package cat.xtec.ioc.utils;

public class Settings {

    // Mida del joc, s'escalarà segons la necessitat
    public static final int GAME_WIDTH = 240;
    public static final int GAME_HEIGHT = 135;

    // Propietats de la nau
    public static final float SPACECRAFT_VELOCITY = 50;
    public static final int SPACECRAFT_WIDTH = 36;
    public static final int SPACECRAFT_HEIGHT = 15;
    public static final float SPACECRAFT_STARTX = 20;
    public static final float SPACECRAFT_STARTY = GAME_HEIGHT/2 - SPACECRAFT_HEIGHT/2;
    public static final int SPACECRAFT_AMMO = -1;

    //Propietats de la bala
    public static final float BULLET_VELOCITY = 200;
    public static final int BULLET_WIDTH = 6;
    public static final int BULLET_HEIGHT = 6;

    // Rang de valors per canviar la mida de l'asteroide.
    public static final float MAX_ASTEROID = 1.5f;
    public static final float MIN_ASTEROID = 0.5f;

    // Configuració Scrollable
    public static final int ASTEROID_SPEED = -150;
    public static final int ASTEROID_GAP = 75;
    public static final int BG_SPEED = -100;
    public static final int ITEM_SPEED = -90;
    public static final int ITEM_WIDTH = 15;
    public static final int ITEM_HEIGHT = 15;
    public static final int ITEM_GAP = 250;
    public static final int ITEM_AMMO = 3;
}
