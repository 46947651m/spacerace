package cat.xtec.ioc.helpers;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.screens.DifficultyScreen;
import cat.xtec.ioc.screens.GameScreen;

public class DifficultyHandler implements InputProcessor {
    private DifficultyScreen screen;
    private Vector2 stageCoord;
    private Stage stage;
    private SpaceRace game;

    public DifficultyHandler(DifficultyScreen screen, SpaceRace game){
        this.screen = screen;
        stage = screen.getStage();
        this.game = game;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        stageCoord = stage.screenToStageCoordinates(new Vector2(screenX, screenY));
        Actor actorHit = stage.hit(stageCoord.x, stageCoord.y, true);
        if (actorHit != null) {
            if(actorHit.getName().equals("easy")){
                game.setScreen(new GameScreen(stage.getBatch(), stage.getViewport(), 0));
                screen.dispose();
            }else if(actorHit.getName().equals("normal")){
                game.setScreen(new GameScreen(stage.getBatch(), stage.getViewport(), 1));
                screen.dispose();
            }else if(actorHit.getName().equals("hard")){
                game.setScreen(new GameScreen(stage.getBatch(), stage.getViewport(), 2));
                screen.dispose();
            }else{
                //do nothing
                return true;
            }
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
