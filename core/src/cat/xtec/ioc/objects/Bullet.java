package cat.xtec.ioc.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Settings;

public class Bullet extends Actor {
    private Vector2 position;
    private int width, height;
    private Circle collisionCircle;

    public Bullet(float x, float y){
        this.width = Settings.BULLET_WIDTH;
        this.height = Settings.BULLET_HEIGHT;
        this.position = new Vector2(x, y);
        collisionCircle = new Circle(position.x + width / 2.0f, position.y + width / 2.0f, width / 2.0f);
    }

    public Vector2 getPosition() {
        return this.position;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        batch.draw(AssetManager.bulletTex, this.getPosition().x, this.getPosition().y, width, height);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(position.x + Settings.BULLET_VELOCITY*delta <= Settings.GAME_WIDTH) {
            this.position.x += Settings.BULLET_VELOCITY * delta;
            collisionCircle.set(position.x + width / 2.0f, position.y + width / 2.0f, width / 2.0f);
        }else{
            this.remove();
        }
    }

    @Override
    public float getX() { return position.x; }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Circle getCollisionCircle() { return collisionCircle; }
}
