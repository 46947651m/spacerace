package cat.xtec.ioc.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

import java.util.Random;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Settings;

public class Item extends Scrollable{

    private Circle collisionCircle;

    public Item(float x, float y, float width, float height, float velocity) {
        super(x, y, width, height, velocity);
        collisionCircle = new Circle();
        setOrigin();
    }

    public void setOrigin() { this.setOrigin(width/2 + 1, height/2); }

    @Override
    public void act(float delta) {
        super.act(delta);
        collisionCircle.set(position.x + width / 2.0f, position.y + width / 2.0f, width / 2.0f);
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
        position.y =  new Random().nextInt(Settings.GAME_HEIGHT - (int) height);
        setOrigin();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.itemTex, position.x, position.y, width, height);
    }

    public boolean collides(Spacecraft nau) {
        if (position.x <= nau.getX() + nau.getWidth()) {
            return (Intersector.overlaps(collisionCircle, nau.getCollisionRect()));
        }
        return false;
    }
}
