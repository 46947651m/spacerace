package cat.xtec.ioc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.helpers.DifficultyHandler;
import cat.xtec.ioc.helpers.InputHandler;
import cat.xtec.ioc.utils.Settings;

public class DifficultyScreen implements Screen {
    private Stage stage;
    private SpaceRace game;
    private Batch batch;
    private Label.LabelStyle textStyle;
    private Label textLbl, labelEasy, labelNormal, labelHard;
    private Image background;

    public DifficultyScreen(Batch prevBatch, Viewport prevViewport, SpaceRace game){
        this.game = game;
        AssetManager.music.play();
        stage = new Stage(prevViewport, prevBatch);
        batch = stage.getBatch();

        textStyle = new Label.LabelStyle(AssetManager.font, null);
        textLbl = new Label("Select difficulty:", textStyle);
        textLbl.setName("title");
        Container container = new Container(textLbl);
        container.setTransform(true);
        container.center();
        container.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT / 5);
        //container.setName("title");

        labelEasy = new Label("Easy", textStyle);
        labelEasy.setName("easy");
        Container containerEasy = new Container(labelEasy);
        containerEasy.setTransform(true);
        containerEasy.center();
        containerEasy.setPosition(Settings.GAME_WIDTH / 2, (Settings.GAME_HEIGHT / 5)*2);
        //containerEasy.setName("easy");

        labelNormal = new Label("Normal", textStyle);
        labelNormal.setName("normal");
        Container containerNormal = new Container(labelNormal);
        containerNormal.setTransform(true);
        containerNormal.center();
        containerNormal.setPosition(Settings.GAME_WIDTH / 2, (Settings.GAME_HEIGHT / 5)*3);
        //containerNormal.setName("normal");

        labelHard = new Label("Hard", textStyle);
        labelHard.setName("hard");
        Container containerHard = new Container(labelHard);
        containerHard.setTransform(true);
        containerHard.center();
        containerHard.setPosition(Settings.GAME_WIDTH / 2, (Settings.GAME_HEIGHT / 5)*4);
        //containerHard.setName("hard");

        background = new Image(AssetManager.background);
        background.setName("background");
        stage.addActor(background);
        stage.addActor(container);
        stage.addActor(containerEasy);
        stage.addActor(containerNormal);
        stage.addActor(containerHard);

        Gdx.input.setInputProcessor(new DifficultyHandler(this, game));
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

        stage.draw();
        stage.act(delta);
        /*if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(stage.getBatch(), stage.getViewport()));
            dispose();
        }*/
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public Stage getStage() {
        return stage;
    }
}
